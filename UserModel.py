import json

class UserModel (object):

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = password

    def to_json(self):
        return json.dumps(self.__dict__)


    